### Set default editor
export EDITOR=gvim
### Get rid of XDG_RUNTIME_DIR not set' error messages in qutebrowser
#export XDG_RUNTIME_DIR=/tmp/runtime-$USER
### Add node binaries
#export PATH="$PATH${PATH:+:}$HOME/node_modules/.bin"

# Append any additional sh scripts found in $HOME/.profile.d/:
for profile_script in $HOME/.profile.d/*.sh ; do
  if [ -x $profile_script ]; then
    . $profile_script
  fi
done
