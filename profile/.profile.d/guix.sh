export GUIX_PACKAGE_PATH=$HOME/guix-recipes/completed
export GUILE_WARN_DEPRECATED=no

### Add sub-profiles
unset GUIX_PROFILE #This is interfering with the sub-profile scripts, dunno where GUIX_PROFILE is being set. I think in /etc/profile temprarily.
for profile_script in $HOME/.guix-profiles/*/profile/etc/profile ; do
    GUIX_PROFILE=$(dirname $(dirname $profile_script))
    source $profile_script
done
