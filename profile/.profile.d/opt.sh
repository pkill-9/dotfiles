HOME_OPT=$HOME/opt
### Paths to local executables.
export PATH=$HOME/.local/bin:$HOME_OPT/bin:$HOME_OPT/Games/bin${PATH:+:}$PATH

### Add ~/opt data dirs
export XDG_DATA_DIRS="$HOME_OPT/share:$HOME_OPT/Games/share${XDG_DATA_DIRS:+:}$XDG_DATA_DIRS"
