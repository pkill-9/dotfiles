#!/bin/sh

if [ -e /home/itsme/.nix-profile/etc/profile.d/nix.sh ]; then . /home/itsme/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

#Added by me:

NIX_PROFILE=/nix/var/nix/profiles/per-user/$USER/profile
export XDG_DATA_DIRS="$XDG_DATA_DIRS${XDG_DATA_DIRS:+:}$NIX_PROFILE/share"
