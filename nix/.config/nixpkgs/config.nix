{
  # [...]
  allowUnfree = true;
  chromium = {
   enablePepperFlash = true; # Chromium's non-NSAPI alternative to Adobe Flash
   #enableWideVine = true; #not functional apparently
  };

}
